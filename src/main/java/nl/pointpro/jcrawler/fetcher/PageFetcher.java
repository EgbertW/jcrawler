/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.pointpro.jcrawler.fetcher;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;

import nl.pointpro.jcrawler.crawler.Configurable;
import nl.pointpro.jcrawler.crawler.CrawlConfig;
import nl.pointpro.jcrawler.crawler.authentication.AuthInfo;
import nl.pointpro.jcrawler.crawler.authentication.BasicAuthInfo;
import nl.pointpro.jcrawler.crawler.authentication.FormAuthInfo;
import nl.pointpro.jcrawler.crawler.authentication.NtAuthInfo;
import nl.pointpro.jcrawler.crawler.exceptions.PageBiggerThanMaxSizeException;
import nl.pointpro.jcrawler.robotstxt.HostDirectives;
import nl.pointpro.jcrawler.robotstxt.RobotstxtServer;
import nl.pointpro.jcrawler.url.URLCanonicalizer;
import nl.pointpro.jcrawler.url.WebURL;
import nl.pointpro.jcrawler.util.Util;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Yasser Ganjisaffar
 */
public class PageFetcher extends Configurable
{
  protected static final Logger logger = LoggerFactory.getLogger(PageFetcher.class);

  private class HostRequests {
    /** The number of outstanding requests */
    int outstanding = 0;
    /** The next time a request to this host may be made */
    long nextFetchTime = System.currentTimeMillis();
    /** The politeness delay used for this host */
    long delay = config.getPolitenessDelay();
    /** 
     * The penalty to take into account while selecting the best URL to fetch.
     * This value is excluded from the actual waiting time and is increased
     * when it has been selected using getBestURL.
     */
    long penalty = 0;
  }
  
  protected PoolingHttpClientConnectionManager connectionManager;
  protected CloseableHttpClient httpClient;
  protected final Object mutex = new Object();
  protected Map<String, HostRequests> nextFetchTimes;
  protected IdleConnectionMonitorThread connectionMonitorThread = null;
  
  protected long delay_total = 0;
  protected int delay_counter = 0;
  protected long delay_last_time = 0;
  protected long delay_exceeded_last_time = 0;
  
  protected RobotstxtServer robotstxt_server = null;
  
  public PageFetcher(CrawlConfig config) {
    super(config);

    RequestConfig requestConfig =
        RequestConfig.custom().setExpectContinueEnabled(false).setCookieSpec(CookieSpecs.DEFAULT)
                     .setRedirectsEnabled(false).setSocketTimeout(config.getSocketTimeout())
                     .setConnectTimeout(config.getConnectionTimeout()).build();

    RegistryBuilder<ConnectionSocketFactory> connRegistryBuilder = RegistryBuilder.create();
    connRegistryBuilder.register("http", PlainConnectionSocketFactory.INSTANCE);    if (config.isIncludeHttpsPages()) {
      try { // Fixing: https://code.google.com/p/crawler4j/issues/detail?id=174
        // By always trusting the ssl certificate
        SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, new TrustStrategy() {
          @Override
          public boolean isTrusted(final X509Certificate[] chain, String authType) {
            return true;
          }
        }).build();
        SSLConnectionSocketFactory sslsf =
            new SniSSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
        connRegistryBuilder.register("https", sslsf);
      } catch (Exception e) {
        logger.warn("Exception thrown while trying to register https");
        logger.debug("Stacktrace", e);
      }
    }

    Registry<ConnectionSocketFactory> connRegistry = connRegistryBuilder.build();
    connectionManager = new SniPoolingHttpClientConnectionManager(connRegistry);
    connectionManager.setMaxTotal(config.getMaxTotalConnections());
    connectionManager.setDefaultMaxPerRoute(config.getMaxConnectionsPerHost());

    HttpClientBuilder clientBuilder = HttpClientBuilder.create();
    clientBuilder.setDefaultRequestConfig(requestConfig);
    clientBuilder.setConnectionManager(connectionManager);
    clientBuilder.setUserAgent(config.getUserAgentString());
    clientBuilder.setDefaultHeaders(config.getDefaultHeaders());

    if (config.getProxyHost() != null) {
      if (config.getProxyUsername() != null) {
        BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(new AuthScope(config.getProxyHost(), config.getProxyPort()),
                                           new UsernamePasswordCredentials(config.getProxyUsername(),
                                                                           config.getProxyPassword()));
        clientBuilder.setDefaultCredentialsProvider(credentialsProvider);
      }

      HttpHost proxy = new HttpHost(config.getProxyHost(), config.getProxyPort());
      clientBuilder.setProxy(proxy);
      logger.debug("Working through Proxy: {}", proxy.getHostName());
    }

    nextFetchTimes = new HashMap<String, HostRequests>();
    
    httpClient = clientBuilder.build();
    if ((config.getAuthInfos() != null) && !config.getAuthInfos().isEmpty()) {
      doAuthentication(config.getAuthInfos());
    }

    if (connectionMonitorThread == null) {
      connectionMonitorThread = new IdleConnectionMonitorThread(connectionManager);
    }
    connectionMonitorThread.start();
  }
  
  public void setRobotstxtServer(RobotstxtServer server) {
    this.robotstxt_server = server;
  }
  
  /**
   * Set the politeness delay for a specific host.
   * 
   * @param host The host for which to set a delay
   * @param timeout The timeout to set for this host
   */
  public void setPolitenessDelay(String host, long timeout) {
    synchronized (nextFetchTimes) {
      HostRequests req = nextFetchTimes.get(host);
      if (req == null)
        nextFetchTimes.put(host, req = new HostRequests());
      req.delay = timeout;
    }
  }
  
  public long getPolitenessDelay(String host) {
    synchronized (nextFetchTimes) {
      HostRequests req = nextFetchTimes.get(host);
      if (req == null)
        return config.getPolitenessDelay();
      return req.delay;
    }
  }
  
  public long getDefaultPolitenessDelay() {
    return config.getPolitenessDelay();
  }
  
  protected void enforcePolitenessDelay(URL url) {
    if (url == null || (url.getFile() != null && url.getFile().endsWith("robots.txt")))
      return;
    
    String hostname = url.getHost();
    long target;
    synchronized (nextFetchTimes) {
      // Remove pages visited more than the politeness delay ago
      long now = System.currentTimeMillis();
      Iterator<Map.Entry<String, HostRequests>> iterator = nextFetchTimes.entrySet().iterator();
      while (iterator.hasNext()) {
        Map.Entry<String, HostRequests> entry = iterator.next();
        HostRequests host = entry.getValue();
        if (host.nextFetchTime < (now - host.delay) && host.outstanding == 0)
          iterator.remove();
      }
      
      // Find or create a HostRequests struct for this host
      HostRequests host = nextFetchTimes.get(hostname);
      if (host == null) {
        host = new HostRequests();
        
        // Respect crawl-delay in robots.txt if specified, up to
        // a maximum of 60 seconds
        HostDirectives hd = robotstxt_server.getDirectives(url);
        if (hd != null) {
          if (hd.getCrawlDelay() != null) {
            long requested = Math.round(hd.getCrawlDelay() * 1000);
            if (requested > getDefaultPolitenessDelay()) {
              if (requested < 60000) {
                host.delay = requested;
                logger.info("Modified crawl-delay for host {} to {}ms as requested in robots.txt", hostname, host.delay);
              } else {
                host.delay = 60000;
                logger.info("Modified crawl-delay for host {} to 60s - robots.txt requested {}s", hostname, requested / 1000.0);
              }
            }
          }
        }
          
        nextFetchTimes.put(hostname, host);
      }
      
      ++host.outstanding;
      target = host.nextFetchTime;
      
      // The next fetch time is the current time + the politeness delay * the amount of
      // outstanding requests (= threads that are waiting to request a page from this host).
      // One additional std_delay is added in order to compensate for the response time of
      // the host. As soon as all requests have finished, the nextFetchTime is set
      // to the correct value.
      host.nextFetchTime = now + (host.delay * (host.outstanding + 1));
      
      // Remove penalty from the host once it is serving its politeness delay
      host.penalty = Math.max(0, host.penalty - host.delay);
    }
     
    doPolitenessSleep(target - System.currentTimeMillis());
  }

  /**
   * Perform and track a delay spent waiting before a URL can politely
   * be crawled.
   * 
   * @param ms The amount of milliseconds to sleep in this instance
   */
  public void doPolitenessSleep(long ms) {
    Util.sleep(ms);
    
    synchronized (this) {
      ++delay_counter;
      if (ms > 0)
        delay_total += ms;
      politenessLog();
    }
  }
  
  /**
   * Perform and track a delay spent waiting before a URL can politely
   * be crawled.
   * 
   * @param o The object to wait on
   * @param ms The amount of milliseconds to sleep in this instance
   */
  public void doPolitenessWait(Object o, long ms) {
    long waited = Util.wait(o, ms);
    
    synchronized (this) {
      ++delay_counter;
      if (waited > 0)
        delay_total += waited;
      
      politenessLog();
    }
  }
  
  /**
   * Print some info about politeness delay every 5 seconds.
   */
  private void politenessLog() {
    if (delay_last_time < System.currentTimeMillis() - 5000) {
      long avg_msec = Math.round(delay_total / delay_counter);
      
      // Format time
      long tot_sec = Math.round(delay_total / 1000.0);
      long tot_min = tot_sec / 60;
      tot_sec = tot_sec % 60;
      long tot_hr = tot_min / 60;
      tot_min = tot_min % 60;
      StringBuilder b = new StringBuilder();
      if (tot_hr > 0) {
        b.append(tot_hr);
        b.append(":");
      }
      
      b.append(tot_min > 9 ? tot_min : "0" + tot_min);
      b.append(":");
      b.append(tot_sec > 9 ? tot_sec : "0" + tot_sec);
      String duration = b.toString();
      
      double tot_k = Math.round(delay_counter / 100.0) / 10.0;
      logger.info("Politeness sleep: counter: {}k total sleep duration: {} average per call: {}ms", tot_k, duration, avg_msec);
      delay_last_time = System.currentTimeMillis();
    }
  }
  
  /**
   * Mark a request to a host as finished. If this was the last outstanding request
   * for this host, the next fetch time is updated to now() + delay, otherwise
   * it remains untouched.
   * 
   * @param url The parsed URL that has been requested
   */
  private void finishRequest(URL url) {
    if (url == null || (url.getPath() != null && url.getPath().endsWith("robots.txt")))
      return;
    
    String hostname = url.getHost();
    synchronized (nextFetchTimes) {
      HostRequests host = nextFetchTimes.get(hostname);
      if (host != null) {
        if (host.outstanding < 1)
          logger.error("ERROR: outstanding requests for host {} was 0, when calling finishRequest", url.getHost());;
          
        host.outstanding = (host.outstanding > 1 ? host.outstanding - 1: 0);
        if (host.outstanding == 0)
          host.nextFetchTime = System.currentTimeMillis() + host.delay;
          
        nextFetchTimes.put(hostname, host);
      }
    }
  }
  
  private void doAuthentication(List<AuthInfo> authInfos) {
    for (AuthInfo authInfo : authInfos) {
      if (authInfo.getAuthenticationType() == AuthInfo.AuthenticationType.BASIC_AUTHENTICATION) {
        doBasicLogin((BasicAuthInfo) authInfo);
      } else if (authInfo.getAuthenticationType() == AuthInfo.AuthenticationType.NT_AUTHENTICATION) {
        doNtLogin((NtAuthInfo) authInfo);
      } else {
        doFormLogin((FormAuthInfo) authInfo);
      }
    }
  }

  /**
   * BASIC authentication<br/>
   * Official Example: https://hc.apache.org/httpcomponents-client-ga/httpclient/examples/org/apache/http/examples
   * /client/ClientAuthentication.java
   * 
   * @param authInfo The authentication information for the basic login
   * */
  private void doBasicLogin(BasicAuthInfo authInfo) {
    logger.info("BASIC authentication for: " + authInfo.getLoginTarget());
    HttpHost targetHost = new HttpHost(authInfo.getHost(), authInfo.getPort(), authInfo.getProtocol());
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
    credsProvider.setCredentials(new AuthScope(targetHost.getHostName(), targetHost.getPort()),
                                 new UsernamePasswordCredentials(authInfo.getUsername(), authInfo.getPassword()));
    httpClient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
  }

  /**
   * Do NT auth for Microsoft AD sites.
   * 
   * @param authInfo The authentication informatoin for MS AD Authentication
   */
  private void doNtLogin(NtAuthInfo authInfo) {
    logger.info("NT authentication for: " + authInfo.getLoginTarget());
    HttpHost targetHost = new HttpHost(authInfo.getHost(), authInfo.getPort(), authInfo.getProtocol());
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
    try {
      credsProvider.setCredentials(new AuthScope(targetHost.getHostName(), targetHost.getPort()),
              new NTCredentials(authInfo.getUsername(), authInfo.getPassword(),
                      InetAddress.getLocalHost().getHostName(), authInfo.getDomain()));
    } catch (UnknownHostException e) {
      logger.error("Error creating NT credentials", e);
    }
    httpClient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
  }

  /**
   * FORM authentication<br/>
   * Official Example:
   *  https://hc.apache.org/httpcomponents-client-ga/httpclient/examples/org/apache/http/examples/client/ClientFormLogin.java
   *  
   * @param authInfo The authentication information for form authentication
   */
  private void doFormLogin(FormAuthInfo authInfo) {
    logger.info("FORM authentication for: " + authInfo.getLoginTarget());
    String fullUri =
        authInfo.getProtocol() + "://" + authInfo.getHost() + ":" + authInfo.getPort() + authInfo.getLoginTarget();
    HttpPost httpPost = new HttpPost(fullUri);
    List<NameValuePair> formParams = new ArrayList<>();
    formParams.add(new BasicNameValuePair(authInfo.getUsernameFormStr(), authInfo.getUsername()));
    formParams.add(new BasicNameValuePair(authInfo.getPasswordFormStr(), authInfo.getPassword()));

    try {
      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, "UTF-8");
      httpPost.setEntity(entity);
      httpClient.execute(httpPost);
      logger.debug("Successfully Logged in with user: " + authInfo.getUsername() + " to: " + authInfo.getHost());
    } catch (UnsupportedEncodingException e) {
      logger.error("Encountered a non supported encoding while trying to login to: " + authInfo.getHost(), e);
    } catch (ClientProtocolException e) {
      logger.error("While trying to login to: " + authInfo.getHost() + " - Client protocol not supported", e);
    } catch (IOException e) {
      logger.error("While trying to login to: " + authInfo.getHost() + " - Error making request", e);
    }
  }

  public PageFetchResult fetchPage(WebURL webUrl)
      throws InterruptedException, IOException, PageBiggerThanMaxSizeException
  {
    // Getting URL, setting headers & content
    PageFetchResult fetchResult = new PageFetchResult();
    String toFetchURL = webUrl.getURL();
    URL parsedUrl = null;
    try {
      parsedUrl = new URL(toFetchURL);
    } catch (MalformedURLException e)
    {}
    
    HttpUriRequest request = null;
    try {
      request = newHttpUriRequest(toFetchURL);
      // Applying Politeness delay
      enforcePolitenessDelay(parsedUrl);

      CloseableHttpResponse response = httpClient.execute(request);
      fetchResult.setEntity(response.getEntity());
      fetchResult.setResponseHeaders(response.getAllHeaders());
      
      // Setting HttpStatus
      int statusCode = response.getStatusLine().getStatusCode();
      String statusReason = response.getStatusLine().getReasonPhrase();

      // If Redirect ( 3xx )
      if (statusCode == HttpStatus.SC_MOVED_PERMANENTLY || statusCode == HttpStatus.SC_MOVED_TEMPORARILY ||
          statusCode == HttpStatus.SC_MULTIPLE_CHOICES || statusCode == HttpStatus.SC_SEE_OTHER ||
          statusCode == HttpStatus.SC_TEMPORARY_REDIRECT ||
          statusCode == 308) { // todo follow https://issues.apache.org/jira/browse/HTTPCORE-389

        Header header = response.getFirstHeader("Location");
        if (header != null) {
          String movedToUrl = URLCanonicalizer.getCanonicalURL(header.getValue(), toFetchURL);
          fetchResult.setMovedToUrl(movedToUrl);
        }
      } else if (statusCode >= 200 && statusCode <= 299) { // is 2XX, everything looks ok
        fetchResult.setFetchedUrl(toFetchURL);
        String uri = request.getURI().toString();
        if (!uri.equals(toFetchURL)) {
          if (!URLCanonicalizer.getCanonicalURL(uri).equals(toFetchURL)) {
            fetchResult.setFetchedUrl(uri);
          }
        }

        // Checking maximum size
        if (fetchResult.getEntity() != null) {
          long size = fetchResult.getEntity().getContentLength();
          if (size == -1) {
            Header length = response.getLastHeader("Content-Length");
            if (length == null) {
              length = response.getLastHeader("Content-length");
            }
            if (length != null) {
              size = Integer.parseInt(length.getValue());
            }
          }
          if (size > config.getMaxDownloadSize()) {
            response.close();
            throw new PageBiggerThanMaxSizeException(size);
          }
        }
      }

      fetchResult.setStatusCode(statusCode, statusReason);
      return fetchResult;

    } finally { // occurs also with thrown exceptions
      if ((fetchResult.getEntity() == null) && (request != null)) {
        request.abort();
      }
      
      finishRequest(parsedUrl);
    }
  }

  public synchronized void shutDown() {
    if (connectionMonitorThread != null) {
      connectionManager.shutdown();
      connectionMonitorThread.shutdown();
    }
  }

  /**
   * Creates a new HttpUriRequest for the given url. The default is to create a HttpGet without
   * any further configuration. Subclasses may override this method and provide their own logic.
   *
   * @param url the url to be fetched
   * @return the HttpUriRequest for the given url
   */
  protected HttpUriRequest newHttpUriRequest(String url) {
    return new HttpGet(url);
  }

  /**
   * Obtain a list of all registered hosts with the next fetch times, including
   * the penalty based on previous selection of this host.
   * 
   * @return A map of String to Long pairs mapping hostnames to next fetch times
   */
  public Map<String, Long> getHostMap() {
    synchronized (nextFetchTimes) {
      Map<String, Long> hostmap = new HashMap<String, Long>();
    
      for (Map.Entry<String, HostRequests> entry : nextFetchTimes.entrySet()) {
        HostRequests el = entry.getValue();
        hostmap.put(entry.getKey(), el.nextFetchTime + el.penalty);
      }
    
      return hostmap;
    }
  }
  
  public void select(WebURL webURL) {
    synchronized (nextFetchTimes) {
      URI url = webURL.getURI();
      String host = url.getHost();

      HostRequests target_time = nextFetchTimes.get(host);

      if (target_time == null) {
        target_time = new HostRequests();
        target_time.penalty = target_time.delay;
        nextFetchTimes.put(host, new HostRequests());
      } else {
        target_time.penalty += target_time.delay;
      }
    }
  }
  
  public void unselect(WebURL webURL) {
    synchronized (nextFetchTimes) {
      URI url = webURL.getURI();
      String host = url.getHost();

      HostRequests target_time = nextFetchTimes.get(host);
      if (target_time == null)
        return;

      target_time.penalty = Math.max(0,  target_time.penalty - target_time.delay);
    }
  }
}
