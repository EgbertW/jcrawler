package nl.pointpro.jcrawler.parser;

import nl.pointpro.jcrawler.url.WebURL;

import java.util.HashSet;
import java.util.Set;


public class XMLParseData implements ParseData {
  
  private Set<WebURL> outgoingUrls = new HashSet<>();
  private String XMLContent;
  
  
  public String getXMLContent() {
    return XMLContent;
  }

  public void setXMLContent(String textContent) {
    this.XMLContent = textContent;
  }
  
  @Override
  public Set<WebURL> getOutgoingUrls() {

    return outgoingUrls;
  }

  @Override
  public void setOutgoingUrls(Set<WebURL> outgoingUrls) {
    this.outgoingUrls = outgoingUrls; 
  }
}
